package u07asg.utils;

/**
 * Created by Elisa Casadio on 27/04/2017.
 */
public class WatchTime {

    private boolean isRunning;
    private long timer;

    public WatchTime() {
        this.isRunning = false;
    }

    public void start() {
        this.isRunning = true;
        this.timer = System.currentTimeMillis();
    }

    public void stop() {
        this.timer = getTime();
        this.isRunning = false;
    }

    public long getTime() {
        return this.isRunning ? (System.currentTimeMillis() - this.timer) / 1000 : this.timer;
    }
}
