package u07asg;

import u07asg.controller.Controller;
import u07asg.view.MainView;

public class ConnectFourApp {

    public static void main(String[] args){
        MainView view = new MainView();
        ConnectFour connect4 = new ConnectFourImpl("src/u07asg/connect4.pl");
        Controller ctrl = new Controller(view, connect4);
        ctrl.addMainView();
    }
}
