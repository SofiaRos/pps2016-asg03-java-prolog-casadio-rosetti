package u07asg.controller;

import u07asg.ConnectFour;
import u07asg.Player;
import u07asg.players.PlayerAI;
import u07asg.players.PlayerHuman;
import u07asg.view.GameView;
import u07asg.view.LevelView;
import u07asg.view.MainView;
import u07asg.view.ResultView;

import java.util.Optional;
import java.util.concurrent.Semaphore;

/**
 * Created by Sofia Rosetti on 25/04/2017.
 */
public class Controller {

    private static final String RED = "red";
    private final MainView mainView;
    private GameView gameView;
    private LevelView levelView;
    private final ConnectFour connect4;
    private PlayerHuman player1;
    private PlayerHuman player2;
    private PlayerAI playerAI;
    private Semaphore mutexPlayer1;
    private Semaphore mutexPlayer2;
    private long time;
    private boolean isMulti = false;
    private boolean won = true;
    private boolean isFinish = false;
    private boolean isEven = false;

    public Controller(final MainView view, ConnectFour connect4) {
        this.mainView = view;
        this.connect4 = connect4;
    }

    public void addMainView(){
        this.mainView.addController(this);
    }

    public void newMove(int column, Player player){
        int freeCell = this.gameView.checkNextFreeCell(column);
        if (freeCell >= 0 && freeCell != 4) {
            if (this.connect4.move(player, freeCell, column)) {
                this.gameView.addPiece(freeCell, column, player);
            }
        }
    }

    public void newGameSinglePlayer(String color) {
        this.mainView.setVisible(false);
        this.levelView = new LevelView(color);
        this.levelView.addController(this);
    }

    public void newGame(String color, boolean isSingle, boolean isEasy){
        if (isSingle) {
            this.levelView.setVisible(false);
            this.isMulti = false;
            this.gameView = new GameView(color, this.isMulti);
        } else {
            this.mainView.setVisible(false);
            this.isMulti = true;
            this.gameView = new GameView(color, this.isMulti);
        }
        this.gameView.addController(this);
        this.mutexPlayer1 = new Semaphore(color.equals(RED) ? 1 : 0);
        this.mutexPlayer2 = new Semaphore(color.equals(RED) ? 0 : 1);
        this.player1 = createNewPlayer(Player.PlayerX);
        new Thread(player1).start();

        if (isSingle) {
            this.playerAI = new PlayerAI(mutexPlayer2, mutexPlayer1, isEasy, this);
            new Thread(playerAI).start();
        } else {
            this.player2 = createNewPlayer(Player.PlayerO);
            new Thread(player2).start();
        }
    }

    private PlayerHuman createNewPlayer(Player player){
        Semaphore sem1 = player==Player.PlayerX ? mutexPlayer1 : mutexPlayer2;
        Semaphore sem2 = player==Player.PlayerX ? mutexPlayer2 : mutexPlayer1;
        return new PlayerHuman(player, sem1, sem2, this, this.isMulti);
    }

    public void endGame(Player playerWinner, boolean isEven) {
        this.gameView.setVisible(false);
        new ResultView(this.isMulti, playerWinner, isEven, this);
    }

    public PlayerHuman getPlayer(boolean isPlayerX) {
        return isPlayerX ? this.player1 : this.player2;
    }

    public GameView getGameView() {
        return this.gameView;
    }

    public int getScore() {
        return !this.won ? 0 : this.connect4.getScoreMoves();
    }

    public int getBonusScore() {
        return !this.won ? 0 : this.connect4.getScoreTime(this.time);
    }

    public void setTime(long time){
        this.time = time;
    }

    public void setWon() {
        this.won = false;
    }

    public boolean checkVictory() {
        Optional<Player> victory = this.connect4.checkVictory();
        if (victory.isPresent()){
            this.isFinish = true;
            return true;
        }
        if (this.connect4.checkCompleted()){
            this.isFinish = true;
            this.isEven = true;
            return true;
        }
        return false;
    }

    public boolean isFinish() {
        return this.isFinish;
    }

    public boolean isEven() {
        return this.isEven;
    }
}
