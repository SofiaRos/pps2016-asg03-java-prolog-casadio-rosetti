package u07asg.view;

import u07asg.Player;
import u07asg.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sofia Rosetti on 24/04/2017.
 */
public class GameView extends JFrame {

    private static final int NUM_ROWS = 4;
    private static final int NUM_COLUMNS = 4;
    private static final String RED = "red";
    private static final String YELLOW = "yellow";
    private JPanel panel;
    private JPanel panelInput;
    private JLabel [][] grid = new JLabel[NUM_ROWS][NUM_COLUMNS];
    private JButton [][] gridInput = new JButton[1][NUM_COLUMNS];
    private ImageIcon red = new ImageIcon(this.getClass().getResource("/res/red.png"));
    private ImageIcon yellow = new ImageIcon(this.getClass().getResource("/res/yellow.png"));
    private ImageIcon empty = new ImageIcon(this.getClass().getResource("/res/empty.png"));
    private ImageIcon playerIcon;
    private Controller ctrl;


    public GameView(String color, boolean isMulti){
        super("Connect4");
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.panelInput = new JPanel(new GridLayout(1,NUM_COLUMNS));
        this.panel = new JPanel(new GridLayout(NUM_ROWS,NUM_COLUMNS));

        this.playerIcon = new ImageIcon(this.getClass().getResource("/res/" + color + ".png"));

        for (int i = 0; i < 4; i++) {
            this.gridInput[0][i] = new JButton(this.playerIcon);
            this.panelInput.add(this.gridInput[0][i]);
            final int finalI = i;
            this.gridInput[0][i].addActionListener(e -> {
                this.ctrl.getPlayer(true).humanMove(finalI, Player.PlayerX);
                if (isMulti) {
                    this.ctrl.getPlayer(false).humanMove(finalI, Player.PlayerO);
                }
            });
            for (int j = 0; j < 4; j++) {
                this.grid[i][j] = new JLabel(this.empty);
                this.panel.add(this.grid[i][j]);
            }
        }
        this.add(BorderLayout.NORTH,this.panelInput);
        this.add(BorderLayout.CENTER,this.panel);
        this.setSize(400,400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void addPiece(int row, int column, Player player){
        this.grid[row][column].setIcon(this.playerIcon);
        if (player == Player.PlayerX){
            changeColor(YELLOW);
        } else {
            changeColor(RED);
        }
        for (int i = 0; i < 4; i++) {
            this.gridInput[0][i].setIcon(this.playerIcon);
        }
    }

    public void addController(Controller ctrl){
        this.ctrl = ctrl;
    }

    public int checkNextFreeCell(int column){
        for (int i = 3; i >= 0; i--){
            if (this.grid[i][column].getIcon().equals(this.empty)) {
                return i;
            }
        }
        return 4;
    }

    public JLabel[][] getGrid() {
        return this.grid;
    }

    private void changeColor(String color) {
        this.playerIcon = new ImageIcon(this.getClass().getResource("/res/" + color + ".png"));

    }
}