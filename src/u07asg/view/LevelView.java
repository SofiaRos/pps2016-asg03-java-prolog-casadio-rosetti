package u07asg.view;

import u07asg.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Elisa Casadio on 27/04/2017.
 */
public class LevelView extends JFrame {

    private static final String EASY = "Easy";
    private static final String HARD = "Hard";
    private static final String TITLE = "Choose the level";

    private Controller controller;

    public LevelView(String color) {
        super("Connect4");
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel(new GridLayout(3,1,0,30));
        JLabel title = new JLabel(TITLE);
        title.setFont(new Font(null, Font.BOLD, 30));
        title.setHorizontalAlignment(JLabel.CENTER);
        JButton easyButton = createButtonLevel(EASY);
        easyButton.addActionListener(e -> {this.controller.newGame(color, true, true);});
        JButton hardButton = createButtonLevel(HARD);
        hardButton.addActionListener(e -> {this.controller.newGame(color, true, false);});
        panel.add(title);
        panel.add(easyButton);
        panel.add(hardButton);
        panel.setBorder(BorderFactory.createEmptyBorder(10,70,30,70));
        this.add(BorderLayout.CENTER, panel);
        this.setSize(500,350);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void addController(Controller controller) {
        this.controller = controller;
    }

    private JButton createButtonLevel(String text) {
        JButton button = new JButton(text);
        button.setFont(new Font(null, Font.BOLD, 18));
        button.setHorizontalAlignment(JButton.CENTER);
        button.setBackground(text.equals(EASY) ? Color.GREEN : Color.RED);
        return button;
    }
}
