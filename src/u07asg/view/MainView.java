package u07asg.view;

import u07asg.ConnectFour;
import u07asg.ConnectFourImpl;
import u07asg.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sofia Rosetti on 25/04/2017.
 */
public class MainView extends JFrame {

    private ImageIcon red = new ImageIcon(this.getClass().getResource("/res/red.png"));
    private ImageIcon yellow = new ImageIcon(this.getClass().getResource("/res/yellow.png"));
    private final JLabel title;
    private final JButton newSingleGame;
    private final JButton newMultiGame;
    private final ButtonGroup pieceColors;
    private final JRadioButton redPiece;
    private final JRadioButton yellowPiece;
    private final JLabel colorLabel = new JLabel("Choose beginner:");
    private Controller ctrl;


    public MainView(){
        super("Connect4");
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        final GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);
        this.title = new JLabel("CONNECT 4 GAME");
        this.colorLabel.setFont(new Font(null, Font.ITALIC, 18));
        this.pieceColors = new ButtonGroup();
        this.redPiece = new JRadioButton();
        this.redPiece.setSelected(true);
        this.yellowPiece = new JRadioButton();
        this.pieceColors.add(this.redPiece);
        this.pieceColors.add(this.yellowPiece);
        this.title.setFont(new Font(null, Font.BOLD, 20));
        this.newSingleGame = new JButton("Single Player");
        this.newSingleGame.setFont(new Font(null, Font.BOLD, 15));
        this.newMultiGame = new JButton("Multi Player");
        this.newMultiGame.setFont(new Font(null, Font.BOLD, 15));

        this.newSingleGame.addActionListener(e -> {
            if (this.redPiece.isSelected()) {
                ctrl.newGameSinglePlayer("red");
            } else {
                ctrl.newGameSinglePlayer("yellow");
            }
        });

        this.newMultiGame.addActionListener(e -> {
            if (this.redPiece.isSelected()) {
                ctrl.newGame("red", false, false);
            } else {
                ctrl.newGame("yellow", false, false);
            }
        });

        this.addComponent(panel, this.title, 2, 0, 10, 20, 1, layout);
        this.addComponent(panel, this.colorLabel, 2, 1, 10, 0, 1, layout);
        this.addComponent(panel, this.redPiece, 0, 2, 10, 0, 1, layout);
        this.addComponent(panel, new JLabel(this.red), 1, 2, 10, 0, 1, layout);
        this.addComponent(panel, this.yellowPiece, 3, 2, 10, 0, 1, layout);
        this.addComponent(panel, new JLabel(this.yellow), 4, 2, 10, 0, 1, layout);
        this.addComponent(panel, this.newSingleGame, 2, 3, 40, 5, 1, layout);
        this.addComponent(panel, this.newMultiGame, 2, 4, 15, 5, 1, layout);

        this.setPreferredSize(new Dimension(500,350));

        this.add(BorderLayout.NORTH,panel);

        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void addComponent(final JPanel p, final JComponent c,
                              final int x, final int y, final int top,
                              final int bottom, final int width, final GridBagLayout layout){
        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.insets.top = top;
        constraints.insets.bottom = bottom;
        constraints.gridwidth = width;
        constraints.fill = GridBagConstraints.NONE;
        layout.setConstraints(c, constraints);
        p.add(c);
    }

    public void addController(Controller ctrl) {
        this.ctrl = ctrl;
    }

}
