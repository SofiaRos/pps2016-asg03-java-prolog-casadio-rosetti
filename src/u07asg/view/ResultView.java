package u07asg.view;

import u07asg.Player;
import u07asg.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Elisa Casadio on 27/04/2017.
 */
public class ResultView extends JFrame {

    private static final String PLAYER1 = "PLAYER RED";
    private static final String PLAYER2 = "PLAYER YELLOW";
    private static final String EVEN = "EVEN!";
    private static final String SCORE = "Score";
    private static final String BONUS = "Bonus";
    private static final String TOTSCORE = "Total score";
    private Controller controller;

    public ResultView(boolean isMultiplayer, Player playerWinner, boolean isEven, Controller controller) {
        super("Connect4");
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.controller = controller;
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        ImageIcon winImage = new ImageIcon(this.getClass().getResource("/res/win.png"));
        ImageIcon failImage = new ImageIcon(this.getClass().getResource("/res/gameover.png"));
        ImageIcon evenImage = new ImageIcon(this.getClass().getResource("/res/even.png"));
        panel.setBorder(BorderFactory.createEmptyBorder(40,0,0,0));
        if (isMultiplayer || isEven) {
            if (isEven) {
                panel.add(setLabelIcon(evenImage));
                panel.add(setLabelPlayer(EVEN));
            } else {
                panel.add(setLabelIcon(winImage));
                if (playerWinner.equals(Player.PlayerX)) {
                    panel.add(setLabelPlayer(PLAYER1));
                } else {
                    panel.add(setLabelPlayer(PLAYER2));
                }
            }
        } else {
            if (playerWinner.equals(Player.PlayerX)) {
                panel.add(setLabelIcon(winImage));
            } else {
                panel.add(setLabelIcon(failImage));
            }
            panel.add(new JLabel(" "));
            panel.add(setLabelScore(SCORE, this.controller.getScore(), false));
            panel.add(setLabelScore(BONUS, this.controller.getBonusScore(), false));
            panel.add(setLabelScore(TOTSCORE, this.controller.getScore() + this.controller.getBonusScore(), true));
        }
        this.add(BorderLayout.CENTER, panel);
        this.setSize(400,400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private JLabel setLabelIcon(ImageIcon image) {
        JLabel label = new JLabel(image);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        return label;
    }

    private JLabel setLabelPlayer(String text) {
        JLabel label = new JLabel(text);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        switch (text) {
            case PLAYER1:
                label.setForeground(Color.RED);
                break;
            case PLAYER2:
                label.setForeground(Color.YELLOW);
                break;
            default:
                label.setForeground(Color.GREEN);
                break;
        }
        label.setFont(label.getFont().deriveFont(30f));
        return label;
    }

    private JLabel setLabelScore(String text, int value, boolean isTotal) {
        JLabel label = new JLabel(text + ": " + value);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setFont(label.getFont().deriveFont(isTotal ? 20f : 16f));
        return label;
    }

}
