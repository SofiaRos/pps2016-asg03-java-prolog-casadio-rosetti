package u07asg.players

import java.util.Random
import javax.swing.{ImageIcon, JButton, JLabel}

import u07asg.controller.Controller

/**
  * Created by Sofia Rosetti on 27/04/2017.
  */
class AI(board: Array[Array[JLabel]], controller: Controller) {

  var rowPieces: List[Int] = List()
  var columnPieces: List[Int] = List()
  var redString : String = "red.png"
  var yellowString : String = "yellow.png"
  var emptyString : String = "empty.png"

  def getChosenY(): Int = {
    checkMaxRow()
    checkMaxColumn()
    var columnsRate: List[Int] = List()
    for(i <- 0 to 3){
      if (controller.getGameView.checkNextFreeCell(i) == 4) {
        columnsRate = columnsRate ::: List(0)
      } else {
        val nextFreeCell: Int = checkNextFreeCellColumn(i)
        val cellColumnPieces: Int = columnPieces(i)
        val cellRowPieces = rowPieces(nextFreeCell)
        if (cellColumnPieces > cellRowPieces) {
          columnsRate = columnsRate ::: List(cellColumnPieces)
        } else {
          columnsRate = columnsRate ::: List(cellRowPieces)
        }
      }
    }

    val consecutivePiecesPerRow = Array(0,0,0,0)
    for (i <- 3 to 0 by -1) {
      for(j <- 0 to 3) {
        if (equals(board(i)(j),redString)){
          consecutivePiecesPerRow(i) = consecutivePiecesPerRow(i) + 1
        } else if (i != 3) {
          consecutivePiecesPerRow(i) = 0
        }
      }
    }

    for (i <- 3 to 0 by -1) {
      if (consecutivePiecesPerRow(i) == 3) {
        for(j <- 0 to 3) {
          if (equals(board(i)(j), emptyString) && controller.getGameView.checkNextFreeCell(j) != 4) {
            return  j
          }
        }
      }
    }

    val max: Int = columnsRate max
    var index: Int = columnsRate indexOf max
    while (controller.getGameView.checkNextFreeCell(index) == 4) {
      index = new Random().nextInt(4)
    }
    index
  }

  def checkMaxColumn(): Unit = {
    for (i <- 0 to 3) {
      var samePieceNumber: Int = 0
      for (j <- 3 to 0 by -1) {
        if (equals(board(j)(i),redString)){
          samePieceNumber = samePieceNumber + 1
        } else if (equals(board(j)(i),yellowString)){
          samePieceNumber = 0
        }
      }
      columnPieces = columnPieces ::: List(samePieceNumber)
    }
  }

  def checkMaxRow(): Unit = {
    for (i <- 3 to 0 by -1) {
      var samePieceNumber: Int = 0
      for (j <- 0 to 3) {
        if (equals(board(i)(j),redString)){
          samePieceNumber = samePieceNumber + 1
        } else if (equals(board(i)(j),yellowString)){
          samePieceNumber = 0
        }
      }
      rowPieces = rowPieces ::: List(samePieceNumber)
    }
  }

  def checkNextFreeCellColumn(column: Int): Int = {
    val cell: Int = -1
    for (i <- 3 to 0 by -1) {
      if (equals(board(i)(column),emptyString)){
        return i
      }
    }
    cell
  }

  def equals(icon: JLabel, piece: String): Boolean = {
    val iconFileName = icon.getIcon.toString
    val fileName = iconFileName.substring(iconFileName.lastIndexOf("/" )+1)
    if (fileName == piece){
      true
    } else {
      false
    }
  }

}
