package u07asg.players;

import u07asg.Player;
import u07asg.controller.Controller;
import u07asg.utils.WatchTime;

import java.util.concurrent.Semaphore;

/**
 * Created by Elisa Casadio on 25/04/2017.
 */
public class PlayerHuman implements Runnable {

    private final Player player;
    private final Semaphore mineMutex;
    private final Semaphore otherMutex;
    private final Controller controller;
    private boolean isMulti;
    private WatchTime timer;
    private boolean finished = false;
    private boolean isHumanPlayerTurn = false;
    private boolean isWinner = false;

    public PlayerHuman(Player player, Semaphore mine, Semaphore other, Controller controller, boolean isMulti) {
        this.player = player;
        this.mineMutex = mine;
        this.otherMutex = other;
        this.controller = controller;
        this.isMulti = isMulti;
        this.timer = new WatchTime();
    }

    @Override
    public void run() {
        if (!this.isMulti) {
            this.timer.start();
        }
        while (!this.finished) {
            try {
                this.mineMutex.acquire();
                if (!this.controller.isFinish()) {
                    this.isHumanPlayerTurn = true;
                } else {
                    this.finished = true;
                    if (!this.isMulti) {
                        this.timer.stop();
                        this.controller.setTime(this.timer.getTime());
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.isWinner  && !this.controller.isEven()) {
            this.controller.endGame(this.player, false);
        } else if(this.controller.isEven()){
            this.controller.endGame(this.player, true);
        } else if (!this.isMulti){
            this.controller.setWon();
            this.controller.endGame(this.player == Player.PlayerX ? Player.PlayerO : Player.PlayerX, false);
        } else {
            this.otherMutex.release();
        }
    }

    public void humanMove(int y, Player player){
        if (this.controller.getGameView().checkNextFreeCell(y) != 4 && this.isHumanPlayerTurn) {
            this.controller.newMove(y, player);
            this.isWinner = this.controller.checkVictory();
            if (this.isWinner && !this.isMulti) {
                this.timer.stop();
                this.controller.setTime(this.timer.getTime());
            }
            this.otherMutex.release();
            this.isHumanPlayerTurn = false;
        }
    }
}
