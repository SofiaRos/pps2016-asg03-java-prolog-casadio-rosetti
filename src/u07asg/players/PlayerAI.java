package u07asg.players;

import u07asg.controller.Controller;
import u07asg.players.AI;
import u07asg.Player;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by Elisa Casadio on 25/04/2017.
 */
public class PlayerAI implements Runnable {

    private final Semaphore mineMutex;
    private final Semaphore otherMutex;
    private final Controller controller;
    private boolean isEasy;
    private boolean finished = false;

    public PlayerAI (Semaphore mine, Semaphore other, boolean isEasy, Controller controller) {
        this.mineMutex = mine;
        this.otherMutex = other;
        this.isEasy = isEasy;
        this.controller = controller;
    }

    @Override
    public void run() {
        while(!this.finished) {
            try {
                this.mineMutex.acquire();
                if (!this.controller.isFinish()) {
                    if (this.isEasy) {
                        int randomY = new Random().nextInt(4);
                        while (this.controller.getGameView().checkNextFreeCell(randomY) == 4) {
                            randomY = new Random().nextInt(4);
                        }
                        this.controller.newMove(randomY, Player.PlayerO);
                    } else {
                        this.controller.newMove(new AI(this.controller.getGameView().getGrid(), this.controller).getChosenY(), Player.PlayerO);
                    }
                    this.finished = this.controller.checkVictory();
                }
                this.otherMutex.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
